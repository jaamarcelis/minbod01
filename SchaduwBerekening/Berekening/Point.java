import java.io.*;

import org.apache.hadoop.io.*;

public class Point implements Writable 
{
  private DoubleWritable X;
  private DoubleWritable Y;
  private DoubleWritable Z;
  
  public Point() 
  {
    set(new DoubleWritable(0.0), new DoubleWritable(0.0), new DoubleWritable(0.0));
  }
  
  public Point(DoubleWritable x, DoubleWritable y, DoubleWritable z) 
  {
    set(x,y,z);
  }
  
  public Point(String xString, String yString, String zString) 
  {
      DoubleWritable x = new DoubleWritable(Double.parseDouble(xString));
      DoubleWritable y = new DoubleWritable(Double.parseDouble(yString));
      DoubleWritable z = new DoubleWritable(Double.parseDouble(zString));
      set(x,y,z);
  }
  
  public void set(DoubleWritable x, DoubleWritable y, DoubleWritable z) 
  {
    X = x;
    Y = y;
    Z = z;
  }

  
  public DoubleWritable getX() 
  {
    return X;
  }

  public DoubleWritable getY() 
  {
    return Y;
  }
  
  public DoubleWritable getZ() 
  {
    return Z;
  }
  
  public void setZ(DoubleWritable z) 
  {
    Z =z;
  }
  
  public void setZ(double z) 
  {
    Z =new DoubleWritable(Z.get()-z);
  }

  @Override
  public void write(DataOutput out) throws IOException 
  {
    X.write(out);
    Y.write(out);
    Z.write(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException 
  {
    X.readFields(in);
    Y.readFields(in);
    Z.readFields(in);
  }
  
  @Override
  public int hashCode() 
  {
    return X.hashCode() * 163 + Y.hashCode() * 163 + Z.hashCode() * 163;
  }
  
  @Override
  public boolean equals(Object o) 
  {
    if (o instanceof Point) 
    {
      Point point = (Point) o;
      return X.equals(point.X) && Y.equals(point.Y) && Z.equals(point.Z);
    }
    return false;
  }

  @Override
  public String toString() 
  {
    return X + "\t" + Y+"\t"+Z;
  }
}