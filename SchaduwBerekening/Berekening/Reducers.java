import org.apache.hadoop.conf.Configuration;  
import org.apache.hadoop.conf.Configured;  
import org.apache.hadoop.fs.FileSystem;  
import org.apache.hadoop.fs.Path;  
import org.apache.hadoop.io.DoubleWritable; 
import org.apache.hadoop.io.LongWritable; 
import org.apache.hadoop.io.Text; 
import org.apache.hadoop.mapreduce.Job;  
import org.apache.hadoop.mapreduce.Mapper;  
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;  
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;  
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;  
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;  
import org.apache.hadoop.util.Tool;  
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapred.OutputCollector; 
  
import java.io.BufferedReader;  
import java.io.IOException;  
import java.io.InputStreamReader;  
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;   
import java.util.Iterator;

public class Reducers extends MapReduceBase implements Reducer<LongWritable, Point, Text, Text> 
{  
    public static double SunStance;
    private double originLatitude = 52.15517440;
    private double originLongitude = 5.38720621;
    private double originX = 155000.0;
    private double originY = 463000.0;
    public class PQR {

        public PQR(double P, double Q, double R) {
            p = P;
            q = Q;
            r = R;
        }
        public double p;
        public double q;
        public double r;
    }
    
    
    
    private ArrayList<PQR> XpqR = new ArrayList<PQR>(
        Arrays.asList(new PQR[]{
            new PQR(0, 1, 190094.945),
            new PQR(1, 1, -11832.228),
            new PQR(2, 1, -114.221),
            new PQR(0, 3, -32.391),
            new PQR(1, 0, -0.705),
            new PQR(3, 1, -2.340),
            new PQR(1, 3, -0.608),
            new PQR(0, 2, -0.008),
            new PQR(2, 3, 0.148)
        }
    ));

    private ArrayList<PQR> YpqR = new ArrayList<PQR>(
        Arrays.asList(new PQR[]{
            new PQR(1, 0, 309056.544),
            new PQR(0, 2, 3638.893),
            new PQR(2, 0, 73.077),
            new PQR(1, 2, -157.984),
            new PQR(3, 0, 59.788),
            new PQR(0, 1, 0.433),
            new PQR(2, 2, -6.439),
            new PQR(1, 1, -0.032),
            new PQR(0, 4, 0.092),
            new PQR(1, 4, 0.054)
        }
    ));
    
    private ArrayList<PQR> LatpqR = new ArrayList<PQR>(
        Arrays.asList(new PQR[]{
            new PQR(0, 1, 3235.65389),
            new PQR(2, 0, -32.58297),
            new PQR(0, 2, -0.2475),
            new PQR(2, 1, -0.84978),
            new PQR(0, 3, -0.0665),
            new PQR(2, 2, -0.01709),
            new PQR(1, 0, -0.00738),
            new PQR(4, 0, 0.0053),
            new PQR(2, 3, -3.9E-4),
            new PQR(4, 1, 3.3E-4),
            new PQR(1, 1, -1.2E-4)
        }
    ));

    private ArrayList<PQR> LongpqR = new ArrayList<PQR>(
        Arrays.asList(new PQR[]{
            new PQR(1, 0, 5260.52916),
            new PQR(1, 1, 105.94684),
            new PQR(1, 2, 2.45656),
            new PQR(3, 0, -0.81885),
            new PQR(1, 3, 0.05594),
            new PQR(3, 1, -0.05607),
            new PQR(0, 1, 0.01199),
            new PQR(3, 2, -0.00256),
            new PQR(1, 4, 0.00128),
            new PQR(0, 2, 2.2E-4),
            new PQR(2, 0, -2.2E-4),
            new PQR(5, 0, 2.6E-4)
        }
    ));

    
    
    public void reduce(LongWritable key, Iterator<Point> values, OutputCollector<Text, Text> context, Reporter reporter) throws IOException
    {
        Point startingPoint = values.next();
        double lengthDiaginalLine = Math.sqrt(8.0) / (Math.sin(SunStance));
        double shadowDropLength = Math.sqrt( (lengthDiaginalLine*lengthDiaginalLine) - 8.0);
        while(values.hasNext())
        {
            Point point = values.next();            
            if(startingPoint.getZ().get() <= point.getZ().get())
            {
                startingPoint = point;
            }
            else if((startingPoint.getZ().get() - shadowDropLength) < point.getZ().get())
            {
                double difference = startingPoint.getZ().get() - point.getZ().get();
                if(difference < (shadowDropLength*0.5))
                {
                    double[] result = RDToGPS(point.getX().get(),point.getY().get());
                    double x =result[0];
                    double y =result[1];
                    context.collect(new Text((x+"")),new Text((""+y)));
                }
                startingPoint = point;            
            }
            else
            {
                startingPoint.setZ(shadowDropLength);
            }    
        }
    }
    
    public double[] RDToGPS(double x, double y) 
    {
        double deltaX = 1e-5 * (x - originX);
        double deltaY = 1e-5 * (y - originY);

        ArrayList<Double> lijst1 = new ArrayList<Double>();
        ArrayList<Double> lijst2 = new ArrayList<Double>();

        for (PQR pqr : LatpqR) {
            lijst1.add(Weighting(0.0, pqr, deltaX, deltaY) / 3600);
        }

        for (PQR pqr : LongpqR) {
            lijst2.add(Weighting(0.0, pqr, deltaX, deltaY) / 3600);
        }

        return new double[]{originLatitude + reduce(lijst1), originLongitude + reduce(lijst2)};
    }

    public double reduce(ArrayList<Double> a) {
        double result = 0.0;
        for (double d : a) {
            result += d;
        }
        return result;
    }
    
    public Double Weighting(double acc, PQR s, double deltaX, double deltaY) {
        return acc + s.r * Math.pow(deltaX, s.p) * Math.pow(deltaY, s.q);
    }
}  