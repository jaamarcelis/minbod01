public class FunctionLine implements Runnable {
    private Double _angle;
    private Double _b;
    private Double _x;
    private Double _y;


    @Override
    public void run() {

        this._y = 2*Math.tan(degToRad(_angle))*_x + _b;

    }


    public FunctionLine(Double b, Double angle){
        this._b = b;
        this._angle = angle;
    }


    public Double getSolution(Double x){
       _x = x;
       run();
       return this._y;
    }

    private Double degToRad(Double deg){
        return deg * (Math.PI / 180);
    }

    @Override
    public String toString(){

        return String.format("f(x) = %sx + %s",2*Math.tan(degToRad(_angle)),_b);
    }
}


