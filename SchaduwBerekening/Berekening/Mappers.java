import org.apache.hadoop.conf.Configuration;  
import org.apache.hadoop.conf.Configured;  
import org.apache.hadoop.fs.FileSystem;  
import org.apache.hadoop.fs.Path;  
import org.apache.hadoop.io.Text;  
import org.apache.hadoop.mapreduce.Job;  
//import org.apache.hadoop.mapred.Mapper;  
import org.apache.hadoop.mapred.Mapper;  
import org.apache.hadoop.mapred.MapReduceBase;  
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.OutputCollector; 
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;  
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;  
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;  
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;  
import org.apache.hadoop.util.Tool;  
import org.apache.hadoop.util.ToolRunner; 
import org.apache.hadoop.io.DoubleWritable; 
import org.apache.hadoop.io.LongWritable; 
  
import java.io.IOException;  

public class Mappers extends MapReduceBase implements Mapper<LongWritable, Text, LongWritable, Point> 
{  	  
    public static double SunStance;
    public static ProcessData Data;
    
    public void map(LongWritable key, Text value, OutputCollector<LongWritable, Point> context, Reporter reporter) throws IOException
    {
        Double sampleX = 68013.78949;	
        Double sampleY = 447033.0577;
        String line = value.toString();
        String[] parts = line.split(",");        
        Point point = new Point(parts[0], parts[1], parts[2]);
        if(Data == null)
        {
            Data = new ProcessData(sampleX,sampleY,Mappers.SunStance,4000,900);
        }
        long  number = (long)Data.isInShadowOfFunction(point.getX().get(),point.getY().get());
        context.collect(new LongWritable(number), point);        
    } 
}  