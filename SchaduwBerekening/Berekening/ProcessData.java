public class ProcessData {
    private Double _minX;
    private Double _minY;
    private Double _angle;

    public FunctionLine[] _functions;

    public ProcessData(Double minX, Double minY, Double angle, int countX, int countY) {
        _minX = minX;
        _minY = minY;
        _angle = angle;
        _functions = new FunctionLine[countX + countY];

        createFunctions(countX, countY);
    }


    private void createFunctions(int countX, int countY) {
        Double b = _minY - Math.tan(degToRad(_angle)) * _minX;

        Double bTemp = b;

        for (int i = 0; i < countY; i++) {
            _functions[i] = new FunctionLine(bTemp += 2, _angle);
        }

        bTemp = b;

        for (int i = countY; i < countX+countY; i++) {
            _functions[i] = new FunctionLine(bTemp -= 2, _angle);
        }
    }

    private Double degToRad(Double deg) {
        return deg * (Math.PI / 180);
    }

    public int isInShadowOfFunction(Double x, Double y) {
        int index = -1;
        for (int i = 0; i < _functions.length-1; i++) {
            Double firstY = _functions[i].getSolution(x);
            Double secondY = _functions[i + 1].getSolution(x);
            if (y < firstY && y > secondY) {
                index = i;
                break;
            }
        }
        return index;
    }


}
