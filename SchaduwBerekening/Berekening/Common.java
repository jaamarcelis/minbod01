import org.apache.hadoop.conf.Configuration;  
import org.apache.hadoop.conf.Configured;  
import org.apache.hadoop.fs.FileSystem;  
import org.apache.hadoop.fs.Path;     
import org.apache.hadoop.mapred.FileOutputFormat;  
import org.apache.hadoop.mapred.FileInputFormat; 
import org.apache.hadoop.util.ToolRunner;  
import org.apache.hadoop.mapred.JobClient;  
import org.apache.hadoop.mapred.JobConf; 
import org.apache.hadoop.mapreduce.lib.jobcontrol.ControlledJob;
import org.apache.hadoop.mapreduce.lib.jobcontrol.JobControl;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable; 
import org.apache.hadoop.mapreduce.Job; 
import org.apache.hadoop.io.Text;

public class Common
{
    public static String OUT = "output";  
    public static String IN = "input";  
    
    public static void main(String[] args) throws Exception 
    { 
        Double sampleX = 68013.78949;	
        Double sampleY = 447033.0577;
        ProcessData Data = new ProcessData(sampleX,sampleY,Mappers.SunStance,4000,900);
        SunCalculation calculator = new SunCalculation();        
        Configuration conf = new Configuration(); 
        conf.set("mapred.textoutputformat.separator", ",");
        conf.set("mapreduce.output.textoutputformat.separator", ",");
        conf.set("mapreduce.output.key.field.separator", ",");
        conf.set("mapred.textoutputformat.separatorText", ",");
        double zonhoek = calculator.berekenen(args[2])[0];
        Mappers.SunStance = zonhoek;
        Reducers.SunStance = degToRad(Mappers.SunStance);
        IN = args[0];  
        OUT = args[1]; 
        System.out.println("Starting");
  
        String infile = IN; 
        String outputfile = OUT;  
  
        boolean isdone = false;  
        boolean success = false; 
        int counter = 0;
        JobConf job = new JobConf(conf, Common.class);
        job.setJobName("Shadow calculation");
        outputfile = OUT; 
        FileInputFormat.setInputPaths(job, new Path(infile)); 
        FileOutputFormat.setOutputPath(job, new Path(outputfile)); 
        
        job.setMapperClass(Mappers.class);  
        job.setReducerClass(Reducers.class);
            
        job.setMapOutputKeyClass(LongWritable.class);  
        job.setMapOutputValueClass(Point.class);   
            
        job.setOutputKeyClass(Text.class);  
        job.setOutputValueClass(Text.class);              
            
        JobClient.runJob(job);        
        Thread.sleep(100);
        System.out.println("Stopping.....");
        System.exit(0);
    }
    
    private static Double degToRad(Double deg)
    {
        return deg * (Math.PI / 180);
    }
}