public class SunCalculation 
{
    
    public static double phi = 51.50;
    public int[] maanddagen = new int[] {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};   

    public double [] berekenen (String datumtijd) 
    {
        //‎HH-MI-DD-MA-JJJJ
        String[] datumtijden = datumtijd.split("-");
        int uur = Integer.parseInt(datumtijden[0]);
        int minuut = Integer.parseInt(datumtijden[1]);
        int dag = Integer.parseInt(datumtijden[2]);
        int maand = Integer.parseInt(datumtijden[3]);
        int jaar = Integer.parseInt(datumtijden[4]);
        int totaaldag = dagbereken(maand, dag);
        double tijdinuren = tijdbereken ((isWintertijd(maand,dag)? uur + 1: uur),minuut);
        double declinatie = declinatie(totaaldag);
        double uurhoek = uurhoek(tijdinuren);
        double zonhoogte = zonhoogte(uurhoek, declinatie);
        double azimuth = azimuth(uurhoek, declinatie, zonhoogte);
        double[] resultaat = new double[] {zonhoogte, azimuth};
        return resultaat;
    }
    public boolean isWintertijd (int maand, int dag) 
    {
        if(maand < 4 || (maand == 10 && dag > 27 )|| maand > 11) {
            return true;
        }
        return false;
    }
    
    public double tijdbereken(int uur, int minuut) 
    {
        double tijdinuren = uur;
        tijdinuren += (minuut/60);
        return tijdinuren;
    }
    
    public int dagbereken (int maand, int dag) 
    {
        int totaaldag = 0;
        for(int i = 0; i < maand; i++) {
            totaaldag += maanddagen[i];
        }
        totaaldag += dag;   
        return totaaldag;       
    }
    
    public double declinatie (int dag) 
    {
        double declinatie = 23.44 * Math.sin(360*(284 + dag)/365);
        return declinatie;        
    } 
    
    public double uurhoek (double tijd) {
        double uurhoek = tijd * 15;
        return uurhoek;
    }
    
    public double zonhoogte (double uurhoek, double declinatie) 
    {
        //h = arcsin (sin ф sin d – cos ф cos d cos u)
        double zonhoogte = Math.asin(Math.sin(phi) * Math.sin(declinatie) - Math.cos(phi) * Math.cos(declinatie) * Math.cos(uurhoek));
        return zonhoogte;
    }
    
    public double azimuth(double uurhoek, double declinatie, double zonhoogte) {
        //a = arcsin { (cos d sin u) / cos h } 
        double azimuth = Math.asin((Math.cos(declinatie) * Math.sin(uurhoek)) / Math.cos(zonhoogte));
        return azimuth;
    }
  
}

