/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdata;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author Wayne Rijsdijk
 */
public class convert {
    static boolean convertHeights = true;
    
    public static void main(String[] args) {
		System.out.println("Please select a datafile");
		File dataFile = new ChooseFile().getFile();
        
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("test-data.txt", "UTF-8");
            
            double minHeight = 999999999;
            
            int lineNumber = 0;
            StringTokenizer st;
            Scanner scanner = null;
            try {
                if(convertHeights) {
                    scanner = new Scanner(dataFile);
                    while(scanner.hasNextLine()) {
                        lineNumber++;
                        //if(lineNumber>100){break;}
                        if (lineNumber > 1) {

                            String nextLine = scanner.nextLine();
                            if(nextLine == null) { //makes sure that no null value is passed to the StringTokenizer. This is a part of the data validation system.
                                continue;
                            }
                            st = new StringTokenizer(nextLine, ","); //use StringTokenizer, because that's twice as fast as String.split() when using simple patterns
                            
                            try {
                                double x = Double.parseDouble(st.nextToken());
                                double y = Double.parseDouble(st.nextToken());
                                double height = Double.parseDouble(st.nextToken());

                                if(height < minHeight) {
                                    minHeight = height;
                                }
                            }
                            catch(NumberFormatException e) {

                            }
                        }
                    }
                }
                
                lineNumber = 0;
                scanner = new Scanner(dataFile);
                while(scanner.hasNextLine()) {
                    lineNumber++;
                    if (lineNumber > 1) {

                        String nextLine = scanner.nextLine();
                        if(nextLine == null) { //makes sure that no null value is passed to the StringTokenizer. This is a part of the data validation system.
                            continue;
                        }
                        st = new StringTokenizer(nextLine, ","); //use StringTokenizer, because that's twice as fast as String.split() when using simple patterns
                        
                        Coordinate c;
                        RDConverter RDConverter = new RDConverter();
                        try {
                            double x = Double.parseDouble(st.nextToken());
                            double y = Double.parseDouble(st.nextToken());
                            double height = Double.parseDouble(st.nextToken());
                            
                            c = RDConverter.RDToGPS(x, y);
                            
                            x = c.getX();
                            y = c.getY();
                            
                            //System.out.println(x + "," + y + "," + height);
                            writer.println(x + "," + y + "," + height + (convertHeights ? 0 - minHeight : 0));
                        }
                        catch(NumberFormatException e) {
                            
                        }
                    }
                }
            }
            catch(FileNotFoundException e) {
                System.out.println("The selected file could not be found!");
            }
            catch(NumberFormatException e) {
                System.out.println("It look likes the data is corrupted! (Line: " + lineNumber + ")");
                e.getMessage();
                e.getStackTrace();
            }
            catch(Exception e) {
                System.out.println("A unknown error occured: " + e.getMessage());
                e.getStackTrace();
            }
            finally {
                if(scanner != null) {
                    scanner.close();
                }
                writer.close();
                System.out.println("Writer closed!");
            }
        }
        catch(FileNotFoundException | UnsupportedEncodingException e) {}
    }
}
