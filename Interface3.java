package harbourtraffic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.DefaultTileFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointPainter;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;


public class Interface3 extends javax.swing.JDialog implements ActionListener{

// Variables declaration - do not modify
private javax.swing.JLabel titleLbl;
private javax.swing.JPanel buttonPnl;
private javax.swing.JButton closeBtn;
private javax.swing.JLabel dummyLbl;
private javax.swing.JPanel mapContainer;
private javax.swing.JPanel titlePnl;
private javax.swing.JLabel usernameLbl;

private JXMapKit jXMapKit;


// End of variables declaration

/** Creates new form AddServiceDlg */
public Interface3(java.awt.Frame parent, boolean modal) {
super(parent, modal);
initComponents();
}
/** Creates new form AddServiceDlg */
public Interface3() {
setModal(true);
initComponents();
}

private void initComponents() {
java.awt.GridBagConstraints gridBagConstraints;

titlePnl = new javax.swing.JPanel();
titleLbl = new javax.swing.JLabel();
buttonPnl = new javax.swing.JPanel();
closeBtn = new javax.swing.JButton();
dummyLbl = new javax.swing.JLabel();
mapContainer = new javax.swing.JPanel();
usernameLbl = new javax.swing.JLabel();

setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
getContentPane().setLayout(new java.awt.GridBagLayout());

titlePnl.setLayout(new java.awt.GridBagLayout());


gridBagConstraints = new java.awt.GridBagConstraints();
gridBagConstraints.gridx = 0;
gridBagConstraints.gridy = 0;
gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
gridBagConstraints.ipady = 10;
gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
gridBagConstraints.weightx = 1.0;
gridBagConstraints.insets = new java.awt.Insets(2, 4, 2, 4);
titlePnl.add(titleLbl, gridBagConstraints);

gridBagConstraints = new java.awt.GridBagConstraints();
gridBagConstraints.gridx = 0;
gridBagConstraints.gridy = 0;
gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
gridBagConstraints.weightx = 1.0;
getContentPane().add(titlePnl, gridBagConstraints);

buttonPnl.setLayout(new java.awt.GridBagLayout());

gridBagConstraints = new java.awt.GridBagConstraints();
gridBagConstraints.gridx = 2;
gridBagConstraints.gridy = 0;
gridBagConstraints.ipadx = 7;
gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
buttonPnl.add(closeBtn, gridBagConstraints);
gridBagConstraints = new java.awt.GridBagConstraints();
gridBagConstraints.gridx = 0;
gridBagConstraints.gridy = 0;
gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
gridBagConstraints.weightx = 1.0;
buttonPnl.add(dummyLbl, gridBagConstraints);

gridBagConstraints = new java.awt.GridBagConstraints();
gridBagConstraints.gridx = 0;
gridBagConstraints.gridy = 2;
gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
gridBagConstraints.weightx = 1.0;
getContentPane().add(buttonPnl, gridBagConstraints);

mapContainer.setLayout(new java.awt.GridBagLayout());
gridBagConstraints = new java.awt.GridBagConstraints();
gridBagConstraints.gridx = 0;
gridBagConstraints.gridy = 0;
gridBagConstraints.gridwidth = 2;
gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
gridBagConstraints.insets = new java.awt.Insets(5, 7, 0, 7);
mapContainer.add(usernameLbl, gridBagConstraints);

gridBagConstraints = new java.awt.GridBagConstraints();
gridBagConstraints.gridx = 0;
gridBagConstraints.gridy = 1;
gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
gridBagConstraints.weightx = 1.0;
gridBagConstraints.weighty = 1.0;
getContentPane().add(mapContainer, gridBagConstraints);

addCustomAttributes();
registerEventListeners();
initializeMapKit();

setSize(450,300);
setLocationRelativeTo(null);
}


public void addCustomAttributes(){
closeBtn.setText("Close");
titleLbl.setText("OpenStreet Maps");
titlePnl.setBackground(Color.WHITE);
titleLbl.setFont(new Font("Arial",Font.BOLD,11));
titleLbl.setForeground(new Color(65,94,117));
}


public void initializeMapKit(){
jXMapKit= new JXMapKit();

jXMapKit.setDefaultProvider(org.jdesktop.swingx.JXMapKit.DefaultProviders.OpenStreetMaps);
jXMapKit.setAddressLocationShown(false);
jXMapKit.setDataProviderCreditShown(false);
jXMapKit.setCenterPosition(new GeoPosition(0,0));
jXMapKit.setMiniMapVisible(false);
jXMapKit.setZoomSliderVisible(false);
jXMapKit.setZoomButtonsVisible(false);
jXMapKit.setZoom(15);
((DefaultTileFactory)jXMapKit.getMainMap().getTileFactory()).setThreadPoolSize(8);

GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
gridBagConstraints.gridx = 0;
gridBagConstraints.gridy = 0;
gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
gridBagConstraints.weightx = 1.0;
gridBagConstraints.weighty = 1.0;
mapContainer.add(jXMapKit, gridBagConstraints);
}




public void registerEventListeners(){
closeBtn.addActionListener(this);
}

/**
* @param args the command line arguments
*/
public static void main(String args[]) {
java.awt.EventQueue.invokeLater(new Runnable() {
public void run() {
Interface3 dialog = new Interface3(new javax.swing.JFrame(), true);
dialog.addWindowListener(new java.awt.event.WindowAdapter() {
public void windowClosing(java.awt.event.WindowEvent e) {
System.exit(0);
}
});
dialog.setVisible(true);
}
});
}

@Override
public void actionPerformed(ActionEvent e) {
if(e.getSource()==closeBtn){
dispose();
}
}



}