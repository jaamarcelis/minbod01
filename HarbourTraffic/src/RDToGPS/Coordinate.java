/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RDToGPS;

/**
 *
 * @author Wayne Rijsdijk
 */
public class Coordinate {
    public double X;
    public double Y;

    public Coordinate(double x, double y) {
        X = x;
        Y = y;
    }

    public String ToString() {
        return "Coördinaten: X: " + X + " Y: " + Y;
    }
    
    public void setX(double x) {
        this.X = x;
    }
    
    public void setY(double y) {
        this.Y = y;
    }
    
    public double getX() {
        return this.X;
    }
    
    public double getY() {
        return this.Y;
    }
}
