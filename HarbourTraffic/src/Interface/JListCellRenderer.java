package Interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ToolTipManager;

public class JListCellRenderer implements ListCellRenderer {

    private JPanel p, iconPanel;
    private JLabel l, text;
    private ImageIcon icon;

    public JListCellRenderer() {
    }

    @Override
    public Component getListCellRendererComponent(final JList list, final Object value, final int index, final boolean isSelected, final boolean hasFocus) {
    // Get current delay
     int initialDelay = ToolTipManager.sharedInstance().getInitialDelay();

     // Show tool tips immediately
     ToolTipManager.sharedInstance().setInitialDelay(0);

     // Show tool tips after a second
     initialDelay = 0;
     ToolTipManager.sharedInstance().setInitialDelay(initialDelay);
     
        BorderLayout panelLayout = new BorderLayout();
        panelLayout.setHgap(2);
        panelLayout.setVgap(20);
        
        p = new JPanel(panelLayout);
        p.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        
        // icon
        iconPanel = new JPanel();
        l = new JLabel();
        
        if(isSelected) {
            p.setBackground(Color.LIGHT_GRAY);
            iconPanel.setBackground(Color.LIGHT_GRAY);
        }

        // text
        text = new JLabel();
        
        if (value instanceof ListItem) {
          ListItem li = (ListItem) value;
          text.setText(li.text);
          icon = li.image;
          
          l.setToolTipText("Vrachtschip");
          l.setIcon(icon);
          
        } else {
            text.setText((String) value);
        }
        
        iconPanel.add(l, BorderLayout.NORTH);
        p.add(iconPanel, BorderLayout.WEST);
        p.add(text, BorderLayout.CENTER);
       
        int width = list.getWidth();
        // this is just to lure the ta's internal sizing mechanism into action
        if (width > 0)
            text.setSize(width, Short.MAX_VALUE);
        return p;
    }
}