/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import java.awt.event.MouseWheelEvent;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.input.ZoomMouseWheelListenerCursor;

/**
 *
 * @author Wayne Rijsdijk
 */
public class MapViewerZoomMouseWheelListenerCursor extends ZoomMouseWheelListenerCursor {
    Interface frame;
    
    public MapViewerZoomMouseWheelListenerCursor(JXMapViewer mapview, Interface frame) {
        super(mapview);
        this.frame = frame;
    }
    
	@Override
	public void mouseWheelMoved(MouseWheelEvent evt)
	{
        super.mouseWheelMoved(evt);
        frame.jSpinner1.setValue(super.viewer.getZoom());
        
    }
}
