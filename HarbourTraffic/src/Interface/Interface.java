/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import RDToGPS.Coordinate;
import RDToGPS.RDConverter;
import data.DataManager;
import data.Prediction;
import data.Sensor;
import data.Ship;
import data.Vertex;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputListener;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.OSMTileFactoryInfo;
import org.jdesktop.swingx.input.CenterMapListener;
import org.jdesktop.swingx.input.PanMouseInputListener;
import org.jdesktop.swingx.mapviewer.DefaultTileFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.LocalResponseCache;
import org.jdesktop.swingx.mapviewer.TileFactoryInfo;
import org.jdesktop.swingx.mapviewer.WaypointPainter;
import org.jdesktop.swingx.painter.Painter;

/**
 *
 * @author Wayne Rijsdijk
 */
public class Interface extends javax.swing.JFrame implements Runnable {
    final private int standardZoom = 7;
    final ArrayList<Vertex> vertexes = new ArrayList<>();
    
    private Painter<JXMapViewer> polygonOverlay = new Painter<JXMapViewer>() {
        @Override
        public void paint(Graphics2D g, JXMapViewer map, int w, int h) {
            g = (Graphics2D) g.create();
            //convert from viewport to world bitmap
            Rectangle rect = map.getViewportBounds();
            g.translate(-rect.x, -rect.y);

            for(Vertex sector : vertexes){
                //create a polygon
                Polygon poly = new Polygon();
                for(GeoPosition gp : sector.positions) {
                    //convert geo to world bitmap pixel
                    Point2D pt = map.getTileFactory().geoToPixel(gp, map.getZoom());
                    poly.addPoint((int)pt.getX(),(int)pt.getY());
                }
                //do the drawing
                if(sector.highlighted) {
                    g.setColor(new Color(255,0,0,100));
                }
                else {
                    g.setColor(new Color(160,160,160,100));
                }
                g.fill(poly);
                g.setColor(Color.GRAY);
                g.draw(poly);
            }
            g.dispose();
        }
    };
    
    private JXMapViewer mapViewer;
    
    // Variables declaration - do not modify                     
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuItem jMenuItemCredits;
    private javax.swing.JMenuBar jMenuBar1;
    public javax.swing.JSpinner jSpinner1;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JList listPane;
    private javax.swing.JPanel stickPane;
    // End of variables declaration
    
    DataManager dm = new DataManager();
    RDConverter RDConverter = new RDConverter();
    HashMap<String, Ship> shipOverlays = new HashMap<>();
    private Thread t;
    
    long unix_timestamp = this.strDateToUnixTimestamp("31/08/2013 23:59:55");
    long unix_to;
    long start_unix = (int) (System.currentTimeMillis() / 1000L);
    long difference = (int) start_unix - unix_timestamp;
    
    private DefaultListModel model = new DefaultListModel();
    
    /**
     * Creates new form Interface4
     */
    public Interface() {
		TileFactoryInfo info = new OSMTileFactoryInfo();
		DefaultTileFactory tileFactory = new DefaultTileFactory(info);
		tileFactory.setThreadPoolSize(8);

		// Setup local file cache
		File cacheDir = new File(System.getProperty("user.home") + File.separator + ".jxmapviewer2");
		LocalResponseCache.installResponseCache(info.getBaseURL(), cacheDir, false);

		// Setup JXMapViewer
		mapViewer = new JXMapViewer();
		mapViewer.setTileFactory(tileFactory);

		//GeoPosition position = new GeoPosition(50.11, 8.68);
        GeoPosition position = new GeoPosition(51.94502, 4.13544);

		// Set the focus
		mapViewer.setZoom(standardZoom);
		mapViewer.setAddressLocation(position);
	
		// Add interactions
		MouseInputListener mia = new PanMouseInputListener(mapViewer);
		mapViewer.addMouseListener(mia);
		mapViewer.addMouseMotionListener(mia);

		mapViewer.addMouseListener(new CenterMapListener(mapViewer));
		
		mapViewer.addMouseWheelListener(new MapViewerZoomMouseWheelListenerCursor(mapViewer, this));
        
        initComponents();
        
        // Data ophalen uit database.
        DataManager dm = new DataManager();
        dm.connect();
        vertexes.addAll(Arrays.asList(dm.getVertexes()));
        dm.close();
        
        dm.connect();
        Prediction[] predicts = dm.getPrediction();
        Ship st = new Ship();
        st.color = Color.RED;
        st.name = "StenaLine_Predict";
        st.queue = new CircularFifoQueue(1000);
        for(Prediction i : predicts) {
            //System.out.println(i.AIS_X_Position + "," + i.AIS_Y_Position);
            Coordinate c = RDConverter.RDToGPS(i.X, i.Y);   
            double x = c.getX();
            double y = c.getY();
            GeoPosition g = new GeoPosition(x, y);
            st.queue.add(g);
        }
        this.shipOverlays.put(st.name, st);
        dm.close();
        
        t = new Thread(this);
        t.start();
    }
                         
    private void initComponents() {
        stickPane = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();
        scrollPane = new javax.swing.JScrollPane();
        listPane = new javax.swing.JList(model);
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenuItemCredits = new javax.swing.JMenuItem("Credits");
        jMenuItemCredits.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                /* Create and display the credits form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        new AboutDialog().setVisible(true);
                    }
                });
            }
        });
        
        
        scrollPane.setViewportView(listPane);
        //listPane.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        //listPane.setCellRenderer(new JListCellRenderer());
        listPane.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    System.out.println("selection");
                }
            }
        });
        
        jSpinner1.setValue(standardZoom);
        jSpinner1.addChangeListener(new InterfaceMapViewerZoomListener(mapViewer));
        mapViewer.addKeyListener(new InterfaceMapViewerKeyListener(mapViewer, jSpinner1));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(800, 600));
        setPreferredSize(new java.awt.Dimension(800, 600));

        stickPane.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        stickPane.setMaximumSize(new java.awt.Dimension(150, 100));
        stickPane.setMinimumSize(new java.awt.Dimension(150, 100));

        jLabel1.setText("Zoom:");

        javax.swing.GroupLayout stickPaneLayout = new javax.swing.GroupLayout(stickPane);
        stickPane.setLayout(stickPaneLayout);
        stickPaneLayout.setHorizontalGroup(
            stickPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stickPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(90, Short.MAX_VALUE))
        );
        stickPaneLayout.setVerticalGroup(
            stickPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stickPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(stickPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(90, Short.MAX_VALUE))
        );

        scrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        scrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(mapViewer);
        mapViewer.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 614, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jMenu2.setText("File");
        jMenuBar1.add(jMenu2);

        jMenu4.setText("View");
        jMenuBar1.add(jMenu4);

        jMenu3.setText("Help");
        jMenu3.add(jMenuItemCredits);
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(mapViewer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stickPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(stickPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE))
            .addComponent(mapViewer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }
    
    @Override
    public void run() {
        while (true) {
            repaint();
            try {
                HashMap<String, ListItem> lis = new HashMap();
                dm.connect();
                Sensor[] sensors = dm.getSensorAIS(unix_timestamp);
                //System.out.println(sensors.length);
                for(Sensor i : sensors) {
                    Ship s;
                    if(this.shipOverlays.containsKey(i.AISName)) {
                        s = this.shipOverlays.get(i.AISName);
                        
                    }
                    else {
                        s = new Ship();
                        s.color = Color.LIGHT_GRAY;
                        s.name = i.AISName;
                        s.queue = new CircularFifoQueue(15);
                    }
                    
                    //System.out.println(i.AIS_X_Position + "," + i.AIS_Y_Position);
                    Coordinate c = RDConverter.RDToGPS(i.AIS_X_Position, i.AIS_Y_Position);   
                    double x = c.getX();
                    double y = c.getY();
                    GeoPosition g = new GeoPosition(x, y);
                    s.queue.add(g);
                    
                    this.shipOverlays.put(i.AISName, s);
                    
                    if(!lis.containsKey(i.AISName) && model.size() < 400) {
                        ListItem li_new = new ListItem();
                        li_new.image = new ImageIcon("schip.png");
                        li_new.image.setDescription("Vrachtschip");
                        li_new.text = i.AISName;
                        lis.put(i.AISName, li_new);
                        model.addElement(li_new);
                    }
                }
                dm.close();
                
                if(listPane.getCellRenderer().getClass() != JListCellRenderer.class) {
                    listPane.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                    listPane.setCellRenderer(new JListCellRenderer());
                    
                    listPane.invalidate();
                    listPane.validate();
                }

                Set<MyWaypoint> waypoints = new HashSet();
                Iterator<String> i = shipOverlays.keySet().iterator();
                while(i.hasNext()) {
                    String key = i.next();
                    Ship s2 = shipOverlays.get(key);
                    Iterator<GeoPosition> j = s2.queue.iterator();
                    while(j.hasNext()) {
                        GeoPosition g = j.next();
                        System.out.println(s2.name  + " | " + s2.queue.element().getLatitude() + "," + s2.queue.element().getLongitude());
                        waypoints.add(
                            new MyWaypoint(s2.name.substring(0,1), s2.color, g)
                        );
                    }
                }

                // Create a waypoint painter that takes all the waypoints
                WaypointPainter<MyWaypoint> waypointPainter = new WaypointPainter<>();
                waypointPainter.setWaypoints(waypoints);
                waypointPainter.setRenderer(new FancyWaypointRenderer());
                
                //set overlay so that all the markers will be cleared and repainted
                mapViewer.addOverlayPainter(waypointPainter);
                mapViewer.addOverlayPainter(polygonOverlay);

                //repaint the JXMapViewer
                mapViewer.repaint();

                System.out.println("Thread runned...");
                
                unix_timestamp += 5;
                t.sleep(10000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    private long strDateToUnixTimestamp(String dt) {
        DateFormat formatter;
        Date date;
        long unixtime = 0;
        formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        try {
            date = formatter.parse(dt);
            unixtime = date.getTime() / 1000L;
        }
        catch (ParseException ex) {}
        return unixtime;
    }
}
