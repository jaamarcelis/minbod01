/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import javax.swing.JSpinner;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.input.PanKeyListener;

/**
 *
 * @author Wayne Rijsdijk
 */
public class InterfaceMapViewerKeyListener extends PanKeyListener {
    private JSpinner spinner;
    
    public InterfaceMapViewerKeyListener(JXMapViewer mapViewer, JSpinner spinner) {
        super(mapViewer);
        this.spinner = spinner;
    }
    
    @Override
	public void keyPressed(KeyEvent e)
	{
        System.out.println(this.spinner.getValue() + " | Keycode: " + e.getKeyCode() + " | " + KeyEvent.VK_SUBTRACT + " | " + this.viewer.getZoom());
        
        int zoomLevel = this.viewer.getZoom();
		int delta_x = 0;
		int delta_y = 0;

		switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                delta_x = -OFFSET;
                break;
            case KeyEvent.VK_RIGHT:
                delta_x = OFFSET;
                break;
            case KeyEvent.VK_UP:
                delta_y = -OFFSET;
                break;
            case KeyEvent.VK_DOWN:
                delta_y = OFFSET;
                break;
            case KeyEvent.VK_EQUALS:
                if ((e.getModifiers() & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK) {
                    System.out.println("PLUS ENTERED");
                    zoomLevel--;
                    this.viewer.setZoom(zoomLevel);
                    this.spinner.setValue(zoomLevel);
                }
                break;
            case KeyEvent.VK_MINUS:
                System.out.println("MINUS ENTERED");
                zoomLevel++;
                this.viewer.setZoom(zoomLevel);
                this.spinner.setValue(zoomLevel);
                break;
            case KeyEvent.VK_ADD:
                zoomLevel--;
                this.viewer.setZoom(zoomLevel);
                this.spinner.setValue(zoomLevel);
                break;
            case KeyEvent.VK_SUBTRACT:
                zoomLevel++;
                this.viewer.setZoom(zoomLevel);
                this.spinner.setValue(zoomLevel);
                break;
		}
        
		if (delta_x != 0 || delta_y != 0)
		{
			Rectangle bounds = this.viewer.getViewportBounds();
			double x = bounds.getCenterX() + delta_x;
			double y = bounds.getCenterY() + delta_y;
			this.viewer.setCenter(new Point2D.Double(x, y));
			this.viewer.repaint();
		}
    }
}
