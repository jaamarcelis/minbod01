/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jdesktop.swingx.JXMapViewer;

/**
 *
 * @author Wayne Rijsdijk
 */
public class InterfaceMapViewerZoomListener implements ChangeListener {
    JXMapViewer mapViewer;
    
    public InterfaceMapViewerZoomListener(JXMapViewer mapViewer) {
        this.mapViewer = mapViewer;
    }
    
    @Override
    public void stateChanged(ChangeEvent e) {
        JSpinner s = (JSpinner )e.getSource();
        int spinnerValue = (Integer) s.getValue();
        this.mapViewer.setZoom(spinnerValue);
        
        s.setValue(this.mapViewer.getZoom());
        this.mapViewer.getZoom();
    }
}
