package Interface;

import java.awt.*;
import javax.swing.*;

public class AboutDialog extends JFrame {
    private JPanel panel;
    private JLabel jcomp4;
    private JLabel jcomp5;
    private JLabel jcomp6;
    private JLabel jcomp7;
    private JLabel jcomp8;
    private JLabel jcomp9;
    private JLabel jcomp10;
    private ImageIcon logo;
    private ImageIcon p1;
    private ImageIcon p2;
    private ImageIcon p3;
    private ImageIcon p4;
    private ImageIcon p5;

    public AboutDialog() {
        this.setName("HarbourTraffic - About");
        //this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        
        //construct components
        logo = new ImageIcon("logo.png");
        p1 = new ImageIcon("wayne.png");
        p2 = new ImageIcon("josephine.png");
        p3 = new ImageIcon("remco.png");
        p4 = new ImageIcon("antoon.png");
        p5 = new ImageIcon("jonathan.jpg");
        
        panel = new JPanel();
        jcomp4 = new JLabel ("");
        jcomp4.setHorizontalAlignment(JLabel.CENTER);
        jcomp4.setIcon(logo);
        jcomp5 = new JLabel ("Programmeurs");
        jcomp6 = new JLabel ("Persoon 1");
        jcomp6.setIcon(p1);
        jcomp7 = new JLabel ("Persoon 2");
        jcomp7.setIcon(p2);
        jcomp8 = new JLabel ("Persoon 3");
        jcomp8.setIcon(p3);
        jcomp9 = new JLabel ("Persoon 4");
        jcomp9.setIcon(p4);
        jcomp10 = new JLabel ("Persoon 5");
        jcomp10.setIcon(p5);

        //adjust size and set layout
        panel.setPreferredSize (new Dimension (800, 384));
        panel.setLayout (null);

        //add components
        panel.add (jcomp4);
        panel.add (jcomp5);
        panel.add (jcomp6);
        panel.add (jcomp7);
        panel.add (jcomp8);
        panel.add (jcomp9);
        panel.add (jcomp10);

        //set component bounds (only needed by Absolute Positioning)
        jcomp4.setBounds (5, 5, 790, 155);
        jcomp5.setBounds (5, 165, 100, 25);
        jcomp6.setBounds (5, 185, 150, 150);
        jcomp7.setBounds (165, 185, 150, 150);
        jcomp8.setBounds (325, 185, 150, 150);
        jcomp9.setBounds (485, 185, 150, 150);
        jcomp10.setBounds (645, 185, 150, 150);
        
        this.add(panel);
        this.pack();
        
        //center dialog
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        
        this.setVisible (true);
    }
}