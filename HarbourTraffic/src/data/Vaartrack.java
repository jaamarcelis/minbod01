/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Wayne Rijsdijk
 */
public class Vaartrack
{
    public int Blocked;
    public int Length;
    public int Length_Blocked;
    public int Breadth;
    public int Breadth_Blocked;
    public int X_Position;
    public int Y_Position;
    public int Speed;
    public int Speed_Blocked;
    public int X_Speed;
    public int X_Speed_Blocked;
    public int Y_Speed;
    public int Y_Speed_Blocked;
    public int Orientation;
    public int Orientation_Blocked;
    public String Lat_Position_Track;
    public String Lon_Position_Track;
    public String LatPosition_Anchor;
    public String LonPosition_Anchor;
    public Date UTC_Time;
    public int Anchor_Number;
    public int Identification_ID;
    public int Sensor_ID;

    public Vaartrack(ResultSet rs) 
    {
        try {
            this.Blocked = rs.getInt("Blocked");
            this.Length = rs.getInt("Length");
            this.Length_Blocked = rs.getInt("Length_Blocked");
            this.Breadth = rs.getInt("Breadth");
            this.Breadth_Blocked = rs.getInt("Breadth_Blocked");
            this.X_Position = rs.getInt("X_Position");
            this.Y_Position = rs.getInt("Y_Position");
            this.Speed = rs.getInt("Speed");
            this.Speed_Blocked = rs.getInt("Speed_Blocked");
            this.X_Speed = rs.getInt("X_Speed");
            this.X_Speed_Blocked = rs.getInt("X_Speed_Blocked");
            this.Y_Speed = rs.getInt("Y_Speed");
            this.Y_Speed_Blocked = rs.getInt("Y_Speed_Blocked");
            this.Orientation = rs.getInt("Orientation");
            this.Orientation_Blocked = rs.getInt("Orientation_Blocked");
            this.Lat_Position_Track = rs.getString("Lat_Position_Track");
            this.Lon_Position_Track = rs.getString("Lon_Position_Track");
            this.LatPosition_Anchor = rs.getString("LatPosition_Anchor");
            this.LonPosition_Anchor = rs.getString("LonPosition_Anchor");
            this.UTC_Time = rs.getDate("UTC_Time");
            this.Anchor_Number = rs.getInt("Anchor_Number");
            this.Identification_ID = rs.getInt("IdentificationID");
            this.Sensor_ID = rs.getInt("Sensor_ID");      
        } catch (SQLException ex) {}
    }
    
    public Vaartrack()
    {
        Blocked = 0;
        Length = 0;
        Length_Blocked = 0;
        Breadth = 0;
        Breadth_Blocked = 0;
        X_Position = 0;
        Y_Position = 0;
        Speed = 0;
        Speed_Blocked = 0;
        X_Speed = 0;
        X_Speed_Blocked = 0;
        Y_Speed = 0;
        Y_Speed_Blocked = 0;
        Orientation = 0;
        Orientation_Blocked = 0;
        Lat_Position_Track = "";
        Lon_Position_Track = "";
        LatPosition_Anchor = "";
        LonPosition_Anchor = "";
        UTC_Time = new Date();
        Anchor_Number = 0;
        Identification_ID = 0;
        Sensor_ID = 0;
    }

    public String toSQL()
    {
        String anchorString = Anchor_Number + "";
        if (Anchor_Number == -1)
        {
            anchorString = "NULL";
        }
        String identificationString = Identification_ID + "";
        if (Identification_ID == -1 || Identification_ID == 0)
        {
            identificationString = "NULL";
        }
        String sensorString = Sensor_ID + "";
        if (Sensor_ID == -1 || Sensor_ID == 0)
        {
            sensorString = "NULL";
        }

        return "Vaartrack VALUES(" +
               Blocked + ", " +
               Length + ", " +
               Length_Blocked + ", " +
               Breadth + ", " +
               Breadth_Blocked + ", " +
               X_Position + ", " +
               Y_Position + ", " +
               Speed + ", " +
               Speed_Blocked + ", " +
               X_Speed + ", " +
               X_Speed_Blocked + ", " +
               Y_Speed + ", " +
               Y_Speed_Blocked + ", " +
               Orientation + ", " +
               Orientation_Blocked + ", '" +
               Lat_Position_Track + "', '" +
               Lon_Position_Track + "', '" +
               LatPosition_Anchor + "', '" +
               LonPosition_Anchor + "', '" +
               UTC_Time + "', " +
               anchorString + ", " +
               identificationString + ", " +
               sensorString + ")";
    }
}