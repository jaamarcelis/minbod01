/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Antoon
 */
public class RecordShip 
{   public int Id;
    public int X_Position;
    public int Y_Position;
    public Date UTC_Time;
    
    public String Name;

    public RecordShip(ResultSet rs) 
    {
        try 
        {
            this.Id = rs.getInt("Id");
            this.X_Position = rs.getInt("X-Position");
            this.Y_Position = rs.getInt("Y-Position");
            this.Name = rs.getString("Name");
            
            this.UTC_Time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("UTC-Time"));
           
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
        } 
        catch (ParseException ex) {
        Logger.getLogger(RecordShip.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    
    public RecordShip()
    {
        this.Id = 0;
        this.X_Position = 0;
        this.Y_Position = 0;
        this.Name = "";
        this.UTC_Time = new Date();
        
    }
    
}
