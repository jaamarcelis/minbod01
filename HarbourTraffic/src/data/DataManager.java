package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataManager {

    final String db_host = "213.163.70.198";
    final String db_port = "3306";
    final String db_user = "wayne_minbod";
    final String db_pass = "uivF2rnc";
    final String db_name = "wayne_minbod";

    private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    public void connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            String url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db_name;
            connect = DriverManager.getConnection(url, db_user, db_pass);
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("KAN NIE CONNECTE JONGUH: " + e.getMessage());
            e.getStackTrace();
            this.close();
        }
    }

    public Prediction[] getAnchors() {
        Prediction[] anchors = new Prediction[0];

        String SQL = "SELECT * FROM `Anchor`";

        try {
            this.statement = this.connect.createStatement();
            this.resultSet = this.statement.executeQuery(SQL);

            int rowcount = 0;
            if (this.resultSet.last()) {
                rowcount = this.resultSet.getRow();
                this.resultSet.beforeFirst();
            }
            anchors = new Prediction[rowcount];

            int counter = 0;
            while (this.resultSet.next()) {
                Prediction a = new Prediction(this.resultSet);
                anchors[counter] = a;
                counter++;
            }
        } catch (SQLException ex) {
        }

        return anchors;
    }

    /**
     *
     * @return
     */
    public Vertex[] getVertexes() {
        System.out.println("Data ophalen");
        Vertex[] vertexes = new Vertex[0];
        String SQL = "SELECT "
                + "GROUP_CONCAT(CAST( v.`x` AS CHAR )) as x, "
                + "GROUP_CONCAT(CAST( v.`y` AS CHAR )) as y, "
                + "v.`sector`, "
                + "v.`id`, "
                + "IF(UNIX_TIMESTAMP(h.`time`) - UNIX_TIMESTAMP(\"2014-02-04 12:22:00\") BETWEEN 0 AND 99999, TRUE, FALSE) as highlighted "
                + "FROM `vertexes` AS v "
                + "LEFT JOIN `vertexes_highlighted` AS h ON (v.`id` = h.`vertex_id`) "
                + "GROUP BY v.`id`";

        try {
            this.statement = this.connect.createStatement();
            this.resultSet = this.statement.executeQuery(SQL);

            int rowcount = 0;
            if (this.resultSet.last()) {
                rowcount = this.resultSet.getRow();
                this.resultSet.beforeFirst();
            }
            vertexes = new Vertex[rowcount];

            int counter = 0;
            while (this.resultSet.next()) {
                Vertex a = new Vertex(this.resultSet);
                vertexes[counter] = a;
                counter++;
            }
        } catch (SQLException ex) {
        }

        return vertexes;
    }
    
    public Prediction[] getPrediction() {
        Prediction[] predictions = new Prediction[0];
        String SQL = "SELECT * FROM `Predictions`";
        
        try {
            this.statement = this.connect.createStatement();
            this.resultSet = this.statement.executeQuery(SQL);

            int rowcount = 0;
            if (this.resultSet.last()) {
                rowcount = this.resultSet.getRow();
                this.resultSet.beforeFirst();
            }
            predictions = new Prediction[rowcount];

            int counter = 0;
            while (this.resultSet.next()) {
                Prediction v = new Prediction(this.resultSet);
                predictions[counter] = v;
                counter++;
            }
        } catch (SQLException ex) {
        }

        return predictions;
    }
    
    public Sensor[] getSensorAIS(long unix_timestamp) {
        Sensor[] sensor = new Sensor[0];
        String SQL = "SELECT * "
                + "FROM `SensorAIS` s "
                + "WHERE UNIX_TIMESTAMP(s.`UTC-Time`)-"+unix_timestamp+" BETWEEN -4 AND 0 "
                + "AND s.`AISName` IS NOT NULL "
                + "AND s.`AISName` != '' " 
        + "AND s.`AISNavigationStatus`='Varend op motor' "
        + "GROUP BY s.`UTC-Time`,s.`AISName` "
        + "ORDER BY s.`UTC-Time` DESC";
        
        try {
            this.statement = this.connect.createStatement();
            this.resultSet = this.statement.executeQuery(SQL);

            int rowcount = 0;
            if (this.resultSet.last()) {
                rowcount = this.resultSet.getRow();
                this.resultSet.beforeFirst();
            }
            sensor = new Sensor[rowcount];

            int counter = 0;
            while (this.resultSet.next()) {
                Sensor v = new Sensor(this.resultSet);
                sensor[counter] = v;
                counter++;
            }
        } catch (SQLException ex) {
        }

        return sensor;
    }

    public Vaartrack[] getVaartracksByUnix(long unix_timestamp) {
        Vaartrack[] vaartracks = new Vaartrack[0];

        String SQL = "SELECT * "
                + "FROM `Vaartrack` "
                + "WHERE `UTC-time`=" + unix_timestamp;

        try {
            this.statement = this.connect.createStatement();
            this.resultSet = this.statement.executeQuery(SQL);

            int rowcount = 0;
            if (this.resultSet.last()) {
                rowcount = this.resultSet.getRow();
                this.resultSet.beforeFirst();
            }
            vaartracks = new Vaartrack[rowcount];

            int counter = 0;
            while (this.resultSet.next()) {
                Vaartrack v = new Vaartrack(this.resultSet);
                vaartracks[counter] = v;
                counter++;
            }
        } catch (SQLException ex) {
        }

        return vaartracks;
    }

    /**
     *
     * @date in format yyyy-mm-dd
     */
    public RecordShip[] getShipDateByShipAndDate(int shipId, String date) {
        RecordShip[] records = new RecordShip[0];
        String SQL = String.format("SELECT "
                + " s.`hre_vsl_id` As Id, "
                + " s.`name` as Name, "
                + " vaartrack.`X-Position`, "
                + " vaartrack.`Y-Position`, "
                + " vaartrack.`UTC-Time` "
                + "FROM `visits` v "
                + "JOIN `ships` s ON(v.`hre_vsl_id`=s.`hre_vsl_id`) "
                + "LEFT OUTER JOIN `SensorAIS` sensor ON (s.`name`=sensor.`AISName`) "
                + "LEFT OUTER JOIN `Vaartrack` vaartrack ON (sensor.`Sensor_ID` = vaartrack.`Sensor_ID`) "
                + "WHERE "
                + " v.`start` BETWEEN \"%s 0:00:00\" AND \"%s 23:59:59\" "
                + "AND "
                + " sensor.`AIS-X-Position` IS NOT NULL "
                + "AND "
                + " sensor.`AIS-Y-Position` IS NOT NULL "
                + "AND "
                + " v.`hre_vsl_id`=%s", date, date, shipId);

        try {
            this.statement = this.connect.createStatement();
            this.resultSet = this.statement.executeQuery(SQL);

            int rowcount = 0;
            if (this.resultSet.last()) {
                rowcount = this.resultSet.getRow();
                this.resultSet.beforeFirst();
            }
            records = new RecordShip[rowcount];

            int counter = 0;
            System.out.println(counter);
            while (this.resultSet.next()) {
                RecordShip r = new RecordShip(this.resultSet);
                records[counter] = r;
                counter++;
                System.out.println(counter);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR");
            ex.printStackTrace();
        }
        return records;
    }

    public RecordShip[] getRecordsByUnixTime(String unix_time) {
        RecordShip[] records = new RecordShip[0];
        String SQL = "SELECT Vaartrack.`X-Position` , Vaartrack.`Y-Position` , SensorAIS.`AISName`  "
                + "FROM Vaartrack "
                + "LEFT OUTER JOIN SensorAIS ON Vaartrack.Sensor_ID = SensorAIS.Sensor_ID "
                + "WHERE UNIX_TIMESTAMP(Vaartrack.`UTC-Time`)-UNIX_TIMESTAMP('" + unix_time + "') BETWEEN -4 AND 0 ";

        try {
            this.statement = this.connect.createStatement();
            this.resultSet = this.statement.executeQuery(SQL);

            int rowcount = 0;
            if (this.resultSet.last()) {
                rowcount = this.resultSet.getRow();
                this.resultSet.beforeFirst();
            }
            records = new RecordShip[rowcount];

            int counter = 0;
            System.out.println(counter);
            while (this.resultSet.next()) {
                RecordShip r = new RecordShip(this.resultSet);
                records[counter] = r;
                counter++;
                System.out.println(counter);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR");
            ex.printStackTrace();
        }

        return records;
    }

    /*
     public Vector2D[] getData(int jaar, int kwartaal, String type) {
     Vector2D[] vector;
        
     try {
     Class.forName("com.mysql.jdbc.Driver");
            
     String url = "jdbc:mysql://"+db_host+":"+db_port+"/"+db_name;
     connect = DriverManager.getConnection(url, db_user, db_pass);
            
     int[] months = monthNumbers[kwartaal];
            
     String SQL;
     switch (type) {
     case "min":
     SQL = "SELECT MONTH(`date`)-"+months[0]+" as month, ROUND(MIN(`T`/10),1) as data FROM `weather` WHERE MONTH(`date`) BETWEEN "+months[0]+" AND "+months[1]+" AND YEAR(`date`)="+(2001+jaar)+" GROUP BY YEAR(`date`),MONTH(`date`),DAY(`date`) ORDER BY `date` ASC";
     break;
     case "max":
     SQL = "SELECT MONTH(`date`)-"+months[0]+" as month, ROUND(MAX(`T`/10),1) as data FROM `weather` WHERE MONTH(`date`) BETWEEN "+months[0]+" AND "+months[1]+" AND YEAR(`date`)="+(2001+jaar)+" GROUP BY YEAR(`date`),MONTH(`date`),DAY(`date`) ORDER BY `date` ASC";
     break;
     default:
     SQL = "SELECT MONTH(`date`)-"+months[0]+" as month, ROUND(AVG(`T`/10),1) as data FROM `weather` WHERE MONTH(`date`) BETWEEN "+months[0]+" AND "+months[1]+" AND YEAR(`date`)="+(2001+jaar)+" GROUP BY YEAR(`date`),MONTH(`date`),DAY(`date`) ORDER BY `date` ASC";
     break;
     }
            
     System.out.println(SQL);

     statement = connect.createStatement();
     resultSet = statement.executeQuery(SQL);
            
     int rowcount = 0;
     if (resultSet.last()) {
     rowcount = resultSet.getRow();
     resultSet.beforeFirst();
     }
     vector = new Vector2D[rowcount];
            
     int counter = 0;
     while(resultSet.next()) {
     Vector2D v = new Vector2D(resultSet.getInt(1), resultSet.getInt(2));
     vector[counter] = v;
     counter++;
     }
     } catch (ClassNotFoundException | SQLException e) {
     e.getStackTrace();
     vector = new Vector2D[0];
     } finally {
     close();
     }
            
     return vector;
     }
    
     public Vector2D[] getScatterData(int jaar, int kwartaal, String type1, String type2) {
     Vector2D[] vector;
        
     try {
     Class.forName("com.mysql.jdbc.Driver");
            
     String url = "jdbc:mysql://"+db_host+":"+db_port+"/"+db_name;
     connect = DriverManager.getConnection(url, db_user, db_pass);
            
     int[] months = monthNumbers[kwartaal];
            
     String SQL = "SELECT "+type1+","+type2+" FROM `weather` WHERE MONTH(`date`) BETWEEN "+months[0]+" AND "+months[1]+" AND YEAR(`date`)="+(2001+jaar)+" GROUP BY YEAR(`date`),MONTH(`date`),DAY(`date`) ORDER BY `date` ASC";
     System.out.println(SQL);

     statement = connect.createStatement();
     resultSet = statement.executeQuery(SQL);
            
     int rowcount = 0;
     if (resultSet.last()) {
     rowcount = resultSet.getRow();
     resultSet.beforeFirst();
     }
     vector = new Vector2D[rowcount];
            
     int counter = 0;
     while(resultSet.next()) {
     Vector2D v = new Vector2D(resultSet.getInt(1), resultSet.getInt(2));
     vector[counter] = v;
     counter++;
     }
     } catch (ClassNotFoundException | SQLException e) {
     e.getStackTrace();
     vector = new Vector2D[0];
     } finally {
     close();
     }
            
     return vector;
     }
     */
    public void close() {
        try {
            if (this.resultSet != null) {
                this.resultSet.close();
            }

            if (this.statement != null) {
                this.statement.close();
            }

            if (this.connect != null) {
                this.connect.close();
            }
        } catch (Exception e) {
        }
    }
}
