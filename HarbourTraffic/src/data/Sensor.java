/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Wayne Rijsdijk
 */
public class Sensor
{
    public int Sensor_ID;
    public String Sensor_Type;
    public int AISBlocked;
    public String AISName;
    public int AISName_Blocked;
    public String AISCallSign;
    public int AISCallSign_Blocked;
    public int AIS_COG;
    public int AIS_COG_Blocked;
    public int AIS_SOG;
    public int AIS_SOG_Blocked;
    public int AIS_X_Speed;
    public int AIS_X_Speed_Blocked;
    public int AIS_Y_Speed;
    public int AIS_Y_Speed_Blocked;
    public int AISOrientation;
    public int AISOrientation_Blocked;
    public int AIS_ROT;
    public int AIS_ROT_Blocked;
    public int AISDraught;
    public int AISDraught_Blocked;
    public String AISDestination;
    public int AISDestination_Blocked;
    public Date AIS_ETA;
    public int AIS_ETA_Blocked;
    public String AISNavigationStatus;
    public int AISNavigation_Blocked;
    public String AIS_IMONumber;
    public int AIS_IMONumber_Blocked;
    public String AIS_MMSINumber;
    public int AIS_MMSINumber_Blocked;
    public String AISClass;
    public int AISClass_Blocked;
    public String AISVesselType;
    public int AISVesselType_Blocked;
    public String AISPositionLat;
    public int AISPositionLat_Blocked;
    public String AISPositionLon;
    public int AISPositionLon_Blocked;
    public int AIS_X_Position;
    public int AIS_X_Position_Blocked;
    public int AIS_Y_Position;
    public int AIS_Y_Position_Blocked;
    public int AISLength;
    public int AISLength_Blocked;
    public int AISBreadth;
    public int AISBreadth_Blocked;
    public int AISPersonsOnBoard;
    public int AISPersonsOnBoard_Blocked;

    public Sensor(ResultSet rs) 
    {
        try {
            //this.Sensor_ID = rs.getInt("Sensor_ID");
            //this.Sensor_Type = rs.getString("Sensor_Type");
            //this.AISBlocked = rs.getInt("AISBlocked");
            this.AISName = rs.getString("AISName");
            //this.AISName_Blocked = rs.getInt("AISName_Blocked");
            //this.AISCallSign = rs.getString("AISCallSign");
            //this.AISCallSign_Blocked = rs.getInt("AISCallSign_Blocked");
            //this.AIS_COG = rs.getInt("AIS_COG");
            //this.AIS_COG_Blocked = rs.getInt("AIS_COG_Blocked");
            //this.AIS_SOG = rs.getInt("AIS_SOG");
            //this.AIS_SOG_Blocked = rs.getInt("AIS_SOG_Blocked");
            //this.AIS_X_Speed = rs.getInt("AIS_X_Speed");
            //this.AIS_X_Speed_Blocked = rs.getInt("AIS_X_Speed_Blocked");
            //this.AIS_Y_Speed = rs.getInt("AIS_Y_Speed");
            //this.AIS_Y_Speed_Blocked = rs.getInt("AIS_Y_Speed_Blocked");
            //this.AISOrientation = rs.getInt("AISOrientation");
            //this.AISOrientation_Blocked = rs.getInt("AISOrientation_Blocked");
            //this.AIS_ROT = rs.getInt("AIS_ROT");
            //this.AIS_ROT_Blocked = rs.getInt("AIS_ROT_Blocked");
            //this.AISDraught = rs.getInt("AISDraught");
            //this.AISDraught_Blocked = rs.getInt("AISDraught_Blocked");
            //this.AISDestination = rs.getString("AISDestination");
            //this.AISDestination_Blocked = rs.getInt("AISDestination_Blocked");
            //this.AIS_ETA = rs.getDate("AIS-ETA");
            //this.AIS_ETA_Blocked = rs.getInt("AIS_ETA_Blocked");
            this.AISNavigationStatus = rs.getString("AISNavigationStatus");
            //this.AISNavigation_Blocked = rs.getInt("AISNavigation_Blocked");
            //this.AIS_IMONumber = rs.getString("AIS_IMONumber");
            //this.AIS_IMONumber_Blocked = rs.getInt("AIS_IMONumber_Blocked");
            //this.AIS_MMSINumber = rs.getString("AIS_MMSINumber");
            //this.AIS_MMSINumber_Blocked = rs.getInt("AIS_MMSINumber_Blocked");
            //this.AISClass = rs.getString("AISClass");
            //this.AISClass_Blocked = rs.getInt("AISClass_Blocked");
            //this.AISVesselType = rs.getString("AISVesselType");
            //this.AISVesselType_Blocked = rs.getInt("AISVesselType_Blocked");
            //this.AISPositionLat = rs.getString("AISPositionLat");
            //this.AISPositionLat_Blocked = rs.getInt("AISPositionLat_Blocked");
            //this.AISPositionLon = rs.getString("AISPositionLon");
            //this.AISPositionLon_Blocked = rs.getInt("AISPositionLon_Blocked");
            this.AIS_X_Position = rs.getInt("AIS-X-Position");
            //this.AIS_X_Position_Blocked = rs.getInt("AIS_X_Position_Blocked");
            this.AIS_Y_Position = rs.getInt("AIS-Y-Position");
            //this.AIS_Y_Position_Blocked = rs.getInt("AIS_Y_Position_Blocked");
            //this.AISLength = rs.getInt("AISLength");
            //this.AISLength_Blocked = rs.getInt("AISLength_Blocked");
            //this.AISBreadth = rs.getInt("AISBreadth");
            //this.AISBreadth_Blocked = rs.getInt("AISBreadth_Blocked");
            //this.AISPersonsOnBoard = rs.getInt("AISPersonsOnBoard");
            //this.AISPersonsOnBoard_Blocked = rs.getInt("AISPersonsOnBoard_Blocked");
        } catch (SQLException ex) {
            System.out.println("ERROR IN SENSOR CLASS");
        }
    }
    
    public Sensor()
    {
        Sensor_ID = 0;
        Sensor_Type = "";
        AISBlocked = 0;
        AISName = "";
        AISName_Blocked = 0;
        AISCallSign = "";
        AISCallSign_Blocked = 0;
        AIS_COG = 0;
        AIS_COG_Blocked = 0;
        AIS_SOG = 0;
        AIS_SOG_Blocked = 0;
        AIS_X_Speed = 0;
        AIS_X_Speed_Blocked = 0;
        AIS_Y_Speed = 0;
        AIS_Y_Speed_Blocked = 0;
        AISOrientation = 0;
        AISOrientation_Blocked = 0;
        AIS_ROT = 0;
        AIS_ROT_Blocked = 0;
        AISDraught = 0;
        AISDraught_Blocked = 0;
        AISDestination = "";
        AISDestination_Blocked = 0;
        AIS_ETA = new Date();
        AIS_ETA_Blocked = 0;
        AISNavigationStatus = "";
        AISNavigation_Blocked = 0;
        AIS_IMONumber = "-1";
        AIS_IMONumber_Blocked = 0;
        AIS_MMSINumber = "";
        AIS_MMSINumber_Blocked = 0;
        AISClass = "";
        AISClass_Blocked = 0;
        AISVesselType = "";
        AISVesselType_Blocked = 0;
        AISPositionLat = "";
        AISPositionLat_Blocked = 0;
        AISPositionLon = "";
        AIS_X_Position = 0;
        AIS_X_Position_Blocked = 0;
        AIS_Y_Position = 0;
        AIS_Y_Position_Blocked = 0;
        AISLength = 0;
        AISLength_Blocked = 0;
        AISBreadth = 0;
        AISBreadth_Blocked = 0;
        AISPersonsOnBoard = 0;
        AISPersonsOnBoard_Blocked = 0;
    }

    public String toSQL() {
        if (AIS_IMONumber.length() == 0)
        {
            AIS_IMONumber = -1 + "";
        }
        return "SensorAIS VALUES(" +
               Sensor_ID + ", '" +
               Sensor_Type + "', " +
               AISBlocked + ", '" +
               AISName + "', " +
               AISName_Blocked + ", '" +
               AISCallSign + "', " +
               AISCallSign_Blocked + ", " +
               AIS_COG + ", " +
               AIS_COG_Blocked + ", " +
               AIS_SOG + ", " +
               AIS_SOG_Blocked + ", " +
               AIS_X_Speed + ", " +
               AIS_X_Speed_Blocked + ", " +
               AIS_Y_Speed + ", " +
               AIS_Y_Speed_Blocked + ", " +
               AISOrientation + ", " +
               AISOrientation_Blocked + ", " +
               AIS_ROT + ", " +
               AIS_ROT_Blocked + ", " +
               AISDraught + ", " +
               AISDraught_Blocked + ", '" +
               AISDestination + "', " +
               AISDestination_Blocked + ", '" +
               AIS_ETA + "', " +
               AIS_ETA_Blocked + ", '" +
               AISNavigationStatus + "', " +
               AISNavigation_Blocked + ", " +
               AIS_IMONumber + ", " +
               AIS_IMONumber_Blocked + ", '" +
               AIS_MMSINumber + "', " +
               AIS_MMSINumber_Blocked + ", '" +
               AISClass + "', " +
               AISClass_Blocked + ", '" +
               AISVesselType + "', " +
               AISVesselType_Blocked + ", '" +
               AISPositionLat + "', " +
               AISPositionLat_Blocked + ", '" +
               AISPositionLon + "', " +
               AISPositionLon_Blocked + ", " +
               AIS_X_Position + ", " +
               AIS_X_Position_Blocked + ", " +
               AIS_Y_Position + ", " +
               AIS_Y_Position_Blocked + ", " +
               AISLength + ", " +
               AISLength_Blocked + ", " +
               AISBreadth + ", " +
               AISBreadth_Blocked + ", " +
               AISPersonsOnBoard + ", " +
               AISPersonsOnBoard_Blocked + ")";
    }
}