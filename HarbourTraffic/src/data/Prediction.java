/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Wayne Rijsdijk
 */
public class Prediction
{
    public int ID;
    public int X;
    public int Y;
    public long Time;

    public Prediction()
    {
        ID = 0;
        X = 0;
        Y = 0;
        Time = 0;
    }
    
    public Prediction(ResultSet rs) {
        try {
            this.ID = rs.getInt("ID");
            this.X = rs.getInt("X");
            this.Y = rs.getInt("Y");
            this.Time = rs.getLong("Time");
        } catch (SQLException ex) {}
    }

    public String toSQL()
    {
        return "Prediction VALUES("
            + ID + ", "
            + X + ", "
            + Y + ", "
            + Time + ")";
    }
}