/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.awt.Color;
import java.util.Queue;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 *
 * @author Wayne Rijsdijk
 */
public class Ship {
    public String name;
    public Color color = Color.CYAN;
    public Queue<GeoPosition> queue = new CircularFifoQueue<>(10);
}
