/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Wayne Rijsdijk
 */
public class Anchor
{
    public int Number;
    public String Radius;
    public String XPos;
    public String YPos;

    public Anchor()
    {
        Number = 0;
        Radius = "";
        XPos = "";
        YPos = "";
    }
    
    public Anchor(ResultSet rs) {
        try {
            this.Number = rs.getInt("Anchor_Number");
            this.Radius = rs.getString("Anchor_Radius");
            this.XPos = rs.getString("Anchor_X-Position");
            this.YPos = rs.getString("Anchor_Y-Position");
        } catch (SQLException ex) {}
    }

    public String toSQL()
    {
        return "Anchor VALUES("
            + Number + ", "
            + Radius + ", "
            + XPos + ", "
            + YPos + ")";
    }
}