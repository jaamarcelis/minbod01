/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Wayne Rijsdijk
 */
public class Identification
{
    public int IdentificationID = 0;
    public String Idenfication_Type = "";
    public String Name = "";
    public int Draught = 0;
    public String Destination = "";
    public int IVS_Identification = 0;
    public int Dangerous_Cargo = 0;
    public int Pilot = 0;
    public int Shiptype = 0;

    public Identification(ResultSet rs) 
    {
        try {
            this.IdentificationID = rs.getInt("IdentificationID");
            this.Idenfication_Type = rs.getString("Idenfication_Type");
            this.Name = rs.getString("Name");
            this.Draught = rs.getInt("Draught");
            this.Destination = rs.getString("Destination");
            this.IVS_Identification = rs.getInt("IVS_Identification");
            this.Dangerous_Cargo = rs.getInt("Dangerous_Cargo");
            this.Pilot = rs.getInt("Pilot");
            this.Shiptype = rs.getInt("Shiptype");
        } catch (SQLException ex) {}
    }
    
    public String toSQL()
    {
        return "IdentificationShip VALUES('"
               + IdentificationID + "', '"
               + Idenfication_Type + "', '"
               + Name + "', '"
               + Draught + "', '"
               + Destination + "', '"
               + IVS_Identification + "', '"
               + Dangerous_Cargo + "', '"
               + Pilot + "', '"
               + Shiptype + "')";
    }
}
