package data;

import RDToGPS.Coordinate;
import RDToGPS.RDConverter;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 *
 * @author Remco
 */
public class Vertex {
    
    public int id;
    // Dit zijn RD's
    public String[] x;
    public String[] y;
    
    public GeoPosition[] positions = new GeoPosition[0];
    
    public String sector;
    public boolean highlighted;
    public Vertex (ResultSet rs) {
        try {
            this.id = rs.getInt("id");
            this.x = rs.getString("x").split(",");
            this.y = rs.getString("y").split(",");
            this.sector = rs.getString("sector");
            this.highlighted = rs.getBoolean("highlighted");
        } catch (SQLException ex) {}
        // RD omzetten naar lat lon
        
        positions = new GeoPosition[x.length];
        
        RDConverter RDConverter = new RDConverter();
        int counter = 0;
        for (int i = 0; i < x.length; i++) {
            Coordinate c = RDConverter.RDToGPS(Double.parseDouble(this.x[i]), Double.parseDouble(this.y[i]));
            positions[i] = new GeoPosition(c.getX(), c.getY());
            counter++;
        }
    }
    
    public String toSQL()
    {
        return "Vertex VALUES("
            + this.id + ", "
            + this.positions[0].getLatitude() + ", "
            + this.positions[0].getLongitude()+ ", "
            + this.sector + ", "
            + this.highlighted + ")";
    }    
}
