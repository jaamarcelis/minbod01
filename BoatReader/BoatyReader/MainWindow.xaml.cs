﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;


namespace BoatyReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Anchor> Anchors = new ObservableCollection<Anchor>();
        public ObservableCollection<Identification> Identifications = new ObservableCollection<Identification>();
        public ObservableCollection<Sensor> Sensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Vaartrack> Tracks = new ObservableCollection<Vaartrack>();
        public int counter = 0;
        public MainWindow()
        {
            IEnumerable<String> lines = File.ReadLines("C:\\data\\tracks_20130901_030000.csv");
            List<String[]> liness = new List<string[]>();
            foreach (string line in lines)
            {
                liness.Add(line.Replace("\t", ",").Split(','));
            }
            //Niet elke rij is even lang op 1 of andere manier
            liness.RemoveAt(0);
            foreach (String[] parts in liness)
            {
                getVaartrack(parts);
                if (parts[35] == "J")
                {
                    getAnchor(parts);
                }
                if (parts[15] != "N" && parts[15].Length == 1)
                {
                    getIdentification(parts);
                }
                if (parts[45] != "R")
                {
                    getSensor(parts);
                }
                counter++;
            }
            InitializeComponent();
            string server = "213.163.70.198";
            string database = "wayne_minbod";
            string uid = "wayne_minbod";
            string password = "uivF2rnc";
            string connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            List<Anchor> Bouyes = Anchors.Distinct().ToList();
            foreach(Anchor anchor in Bouyes)
            {
                string query = "INSERT INTO "+anchor.toSQL();
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
            }
            foreach (Sensor sensor in Sensors)
            {
                string query = "INSERT INTO " + sensor.toSQL();
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
            }
            foreach (Identification identification in Identifications)
            {
                string query = "INSERT INTO " + identification.toSQL();
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
            }
            foreach (Vaartrack track in Tracks)
            {
                string query = "INSERT INTO " + track.toSQL();
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
            }
            connection.Close();

            Datas.ItemsSource = null;
            Datas.ItemsSource = Anchors.Distinct();
        }

        private void getSensor(String[] parts)
        {
            Sensor sensor = new Sensor();
            sensor.Sensor_ID = counter;
            sensor.Sensor_Type = parts[48];
            sensor.AISBlocked = Parse(parts[49]);
            sensor.AISName = parts[50];
            sensor.AISName_Blocked = Parse(parts[51]);
            sensor.AISCallSign = parts[52];
            sensor.AISCallSign_Blocked = Parse(parts[53]);
            sensor.AIS_COG = Parse(parts[54]);
            sensor.AIS_COG_Blocked = Parse(parts[55]);
            sensor.AIS_SOG = Parse(parts[56]);
            sensor.AIS_SOG_Blocked = Parse(parts[57]);
            sensor.AIS_X_Speed = Parse(parts[58]);
            sensor.AIS_X_Speed_Blocked = Parse(parts[59]);
            sensor.AIS_Y_Speed = Parse(parts[60]);
            sensor.AIS_Y_Speed_Blocked = Parse(parts[61]);
            sensor.AISOrientation = Parse(parts[62]);
            sensor.AISOrientation_Blocked = Parse(parts[63]);
            sensor.AIS_ROT = Parse(parts[64]);
            sensor.AIS_ROT_Blocked = Parse(parts[65]);
            sensor.AISDraught = Parse(parts[66]);
            sensor.AISDraught_Blocked = Parse(parts[67]);
            sensor.AISDestination = parts[68];
            sensor.AISDestination_Blocked = Parse(parts[69]);
            sensor.AIS_ETA = ParseTime(parts[70], parts);
            sensor.AIS_ETA_Blocked = Parse(parts[71]);
            sensor.AISNavigationStatus = parts[72];
            sensor.AISNavigation_Blocked = Parse(parts[73]);
            sensor.AIS_IMONumber = parts[74];
            sensor.AIS_IMONumber_Blocked = Parse(parts[75]);
            sensor.AIS_MMSINumber = parts[76];
            sensor.AIS_MMSINumber_Blocked = Parse(parts[77]);
            sensor.AISClass = parts[78];
            sensor.AISClass_Blocked = Parse(parts[79]);
            sensor.AISVesselType = parts[80];
            sensor.AISVesselType_Blocked = Parse(parts[81]);
            sensor.AISPositionLat = parts[82];
            sensor.AISPositionLat_Blocked = Parse(parts[83]);
            sensor.AISPositionLon = parts[84];
            sensor.AIS_X_Position = Parse(parts[85]);
            sensor.AIS_X_Position_Blocked = Parse(parts[86]);
            sensor.AIS_Y_Position = Parse(parts[87]);
            sensor.AIS_Y_Position_Blocked = Parse(parts[88]);
            sensor.AISLength = Parse(parts[89]);
            sensor.AISLength_Blocked = Parse(parts[90]);
            sensor.AISBreadth = Parse(parts[91]);
            sensor.AISBreadth_Blocked = Parse(parts[92]);
            sensor.AISPersonsOnBoard = Parse(parts[93]);
            sensor.AISPersonsOnBoard_Blocked = Parse(parts[94]);
            Sensors.Add(sensor);
        }

        private int Parse(string p)
        {
            try
            {
                if (p == "J")
                    return 1;
                else if (p == "N")
                    return 0;

                else
                    return int.Parse(p);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private void getAnchor(string[] parts)
        {
 	        Anchor anchor = new Anchor();
            anchor.Number = int.Parse(parts[36]);
            anchor.Radius = parts[37];
            anchor.XPos = parts[38];
            anchor.YPos = parts[39];
            Anchors.Add(anchor);
        }

        private void getVaartrack(string[] parts)
        {
            Vaartrack vaartrack = new Vaartrack();
            vaartrack.Blocked = Parse(parts[0]);
            vaartrack.Length = Parse(parts[1]);
            vaartrack.Length_Blocked = Parse(parts[2]);
            vaartrack.Breadth = Parse(parts[3]);
            vaartrack.Breadth_Blocked = Parse(parts[4]);
            vaartrack.X_Position = Parse(parts[5]);
            vaartrack.Y_Position = Parse(parts[6]);
            vaartrack.Speed = Parse(parts[7]);
            vaartrack.Speed_Blocked = Parse(parts[8]);
            vaartrack.X_Speed = Parse(parts[9]);
            vaartrack.X_Speed_Blocked = Parse(parts[10]);
            vaartrack.Y_Speed = Parse(parts[11]);
            vaartrack.Y_Speed_Blocked = Parse(parts[12]);
            vaartrack.Orientation = Parse(parts[13]);
            vaartrack.Orientation_Blocked = Parse(parts[14]);
            vaartrack.Lat_Position_Track = parts[42];
            vaartrack.Lon_Position_Track = parts[43];
            vaartrack.LatPosition_Anchor = parts[44];
            vaartrack.LonPosition_Anchor = parts[45];
            vaartrack.UTC_Time = ParseTime(parts[46], parts);
            vaartrack.Anchor_Number = Parse(parts[36]);
            vaartrack.Identification_ID = counter;
            vaartrack.Sensor_ID = counter;
            Tracks.Add(vaartrack);
        }

        private DateTime ParseTime(string p, string[] parts)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
                if (p != "")
                    return DateTime.ParseExact(p, "dd-MM-yyyy HH:mm:ss", provider);
                else
                    return new DateTime();
            }
        

        private void getIdentification(String[] parts)
        {
            Identification identification = new Identification();
            identification.IdentificationID = counter;
            string type = parts[15];
            if (type == "F")
                identification.Idenfication_Type = 0 + "";
            else if (type == "N")
                identification.Idenfication_Type = 1 + "";
            else
                identification.Idenfication_Type = 2 + "";
            identification.Name = parts[16];
            identification.Draught = Parse(parts[17]);

            identification.Destination = parts[18];
            if (parts[19] == "")
                identification.IVS_Identification = counter;
            else
                identification.IVS_Identification = Parse(parts[19]);
            if (parts[22] == "J")
                identification.Dangerous_Cargo = 1;
            else
                identification.Dangerous_Cargo = 0;

            if (parts[23] == "J")
                identification.Pilot = 1;
            else
                identification.Pilot = 0;

            if (parts[20] == "J")
                identification.Shiptype = 0;
            else if (parts[21] == "J")
                identification.Shiptype = 1;
            else if (parts[24] == "J")
                identification.Shiptype = 2;
            else if (parts[25] == "J")
                identification.Shiptype = 3;
            else if (parts[26] == "J")
                identification.Shiptype = 4;
            else if (parts[27] == "J")
                identification.Shiptype = 5;
            else if (parts[28] == "J")
                identification.Shiptype = 6;
            else
                identification.Shiptype = 7;
            if (!Identifications.Where(u => u.IVS_Identification == identification.IVS_Identification).Any())
            {
                Identifications.Add(identification);
            }
        }
    }
}

public class Anchor
{
    public int Number { get; set; }
    public string Radius { get; set; }
    public string XPos { get; set; }
    public string YPos { get; set; }

    public Anchor()
    {
        Number = 0;
        Radius = "";
        XPos = "";
        YPos = "";
    }

    public string toSQL()
    {
        return "Anchor VALUES('"
               + Radius + "', '"
               + XPos + "', '"
               + YPos + "')";
    }
}

public class Sensor
{
  public int Sensor_ID { get; set; }
  public string Sensor_Type { get; set; }
  public int AISBlocked { get; set; }
  public string AISName { get; set; }
  public int AISName_Blocked { get; set; }
  public string AISCallSign { get; set; }
  public int AISCallSign_Blocked { get; set; }
  public int AIS_COG { get; set; }
  public int AIS_COG_Blocked { get; set; }
  public int AIS_SOG { get; set; }
  public int AIS_SOG_Blocked { get; set; }
  public int AIS_X_Speed { get; set; }
  public int AIS_X_Speed_Blocked { get; set; }
  public int AIS_Y_Speed { get; set; }
  public int AIS_Y_Speed_Blocked { get; set; }
  public int AISOrientation { get; set; }
  public int AISOrientation_Blocked { get; set; }
  public int AIS_ROT { get; set; }
  public int AIS_ROT_Blocked { get; set; }
  public int AISDraught { get; set; }
  public int AISDraught_Blocked { get; set; }
  public string AISDestination { get; set; }
  public int AISDestination_Blocked { get; set; }
  public DateTime AIS_ETA { get; set; }
  public int AIS_ETA_Blocked { get; set; }
  public string AISNavigationStatus { get; set; }
  public int AISNavigation_Blocked { get; set; }
  public string AIS_IMONumber { get; set; }
  public int AIS_IMONumber_Blocked { get; set; }
  public string AIS_MMSINumber { get; set; }
  public int AIS_MMSINumber_Blocked { get; set; }
  public string AISClass { get; set; }
  public int AISClass_Blocked { get; set; }
  public string AISVesselType { get; set; }
  public int AISVesselType_Blocked { get; set; }
  public string AISPositionLat { get; set; }
  public int AISPositionLat_Blocked { get; set; }
  public string AISPositionLon { get; set; }
  public int AIS_X_Position { get; set; }
  public int AIS_X_Position_Blocked { get; set; }
  public int AIS_Y_Position { get; set; }
  public int AIS_Y_Position_Blocked { get; set; }
  public int AISLength { get; set; }
  public int AISLength_Blocked { get; set; }
  public int AISBreadth { get; set; }
  public int AISBreadth_Blocked { get; set; }
  public int AISPersonsOnBoard { get; set; }
  public int AISPersonsOnBoard_Blocked { get; set; }

    public Sensor()
    {
        Sensor_ID = 0;
        Sensor_Type = "";
        AISBlocked = 0;
        AISName = "";
        AISName_Blocked = 0;
        AISCallSign = "";
        AISCallSign_Blocked = 0;
        AIS_COG = 0;
        AIS_COG_Blocked = 0;
        AIS_SOG = 0;
        AIS_SOG_Blocked = 0;
        AIS_X_Speed = 0;
        AIS_X_Speed_Blocked = 0;
        AIS_Y_Speed = 0;
        AIS_Y_Speed_Blocked = 0;
        AISOrientation = 0;
        AISOrientation_Blocked = 0;
        AIS_ROT = 0;
        AIS_ROT_Blocked = 0;
        AISDraught = 0;
        AISDraught_Blocked = 0;
        AISDestination = "";
        AISDestination_Blocked = 0;
        AIS_ETA = new DateTime();
        AIS_ETA_Blocked = 0;
        AISNavigationStatus = "";
        AISNavigation_Blocked = 0;
        AIS_IMONumber = "";
        AIS_IMONumber_Blocked = 0;
        AIS_MMSINumber = "";
        AIS_MMSINumber_Blocked = 0;
        AISClass = "";
        AISClass_Blocked = 0;
        AISVesselType = "";
        AISVesselType_Blocked = 0;
        AISPositionLat = "";
        AISPositionLat_Blocked = 0;
        AISPositionLon = "";
        AIS_X_Position = 0;
        AIS_X_Position_Blocked = 0;
        AIS_Y_Position = 0;
        AIS_Y_Position_Blocked = 0;
        AISLength = 0;
        AISLength_Blocked = 0;
        AISBreadth = 0;
        AISBreadth_Blocked = 0;
        AISPersonsOnBoard = 0;
        AISPersonsOnBoard_Blocked = 0;
    }

    public string toSQL()
    {
        return "SensorAIS VALUES('" +
               Sensor_ID + "', '" +
               Sensor_Type + "', '" +
               AISBlocked + "', '" +
               AISName + "', '" +
               AISName_Blocked + "', '" +
               AISCallSign + "', '" +
               AISCallSign_Blocked + "', '" +
               AIS_COG + "', '" +
               AIS_COG_Blocked + "', '" +
               AIS_SOG + "', '" +
               AIS_SOG_Blocked + "', '" +
               AIS_X_Speed + "', '" +
               AIS_X_Speed_Blocked + "', '" +
               AIS_Y_Speed + "', '" +
               AIS_Y_Speed_Blocked + "', '" +
               AISOrientation + "', '" +
               AISOrientation_Blocked + "', '" +
               AIS_ROT + "', '" +
               AIS_ROT_Blocked + "', '" +
               AISDraught + "', '" +
               AISDraught_Blocked + "', '" +
               AISDestination + "', '" +
               AISDestination_Blocked + "', '" +
               AIS_ETA + "', '" +
               AIS_ETA_Blocked + "', '" +
               AISNavigationStatus + "', '" +
               AISNavigation_Blocked + "', '" +
               AIS_IMONumber + "', '" +
               AIS_IMONumber_Blocked + "', '" +
               AIS_MMSINumber + "', '" +
               AIS_MMSINumber_Blocked + "', '" +
               AISClass + "', '" +
               AISClass_Blocked + "', '" +
               AISVesselType + "', '" +
               AISVesselType_Blocked + "', '" +
               AISPositionLat + "', '" +
               AISPositionLat_Blocked + "', '" +
               AISPositionLon + "', '" +
               AIS_X_Position + "', '" +
               AIS_X_Position_Blocked + "', '" +
               AIS_Y_Position + "', '" +
               AIS_Y_Position_Blocked + "', '" +
               AISLength + "', '" +
               AISLength_Blocked + "', '" +
               AISBreadth + "', '" +
               AISBreadth_Blocked + "', '" +
               AISPersonsOnBoard + "', '" +
               AISPersonsOnBoard_Blocked + "')'";
    }
}

public class Identification
{
    public int IdentificationID = 0;
    public string Idenfication_Type = "";
    public string Name = "";
    public int Draught = 0;
    public string Destination = "";
    public int IVS_Identification = 0;
    public int Dangerous_Cargo = 0;
    public int Pilot = 0;
    public int Shiptype = 0;

    public string toSQL()
    {
        return "IdentificationShip VALUES('"
               + IdentificationID + "', '"
               + Idenfication_Type + "', '"
               + Name + "', '"
               + Draught + "', '"
               + Destination + "', '"
               + IVS_Identification + "', '"
               + Dangerous_Cargo + "', '"
               + Pilot + "', '"
               + Shiptype + "')";
    }
}

public class Vaartrack
{
    public int Blocked { get; set; }
    public int Length { get; set; }
    public int Length_Blocked { get; set; }
    public int Breadth { get; set; }
    public int Breadth_Blocked { get; set; }
    public int X_Position { get; set; }
    public int Y_Position { get; set; }
    public int Speed { get; set; }
    public int Speed_Blocked { get; set; }
    public int X_Speed { get; set; }
    public int X_Speed_Blocked { get; set; }
    public int Y_Speed { get; set; }
    public int Y_Speed_Blocked { get; set; }
    public int Orientation { get; set; }
    public int Orientation_Blocked { get; set; }
    public string Lat_Position_Track { get; set; }
    public string Lon_Position_Track { get; set; }
    public string LatPosition_Anchor { get; set; }
    public string LonPosition_Anchor { get; set; }
    public DateTime UTC_Time { get; set; }
    public int Anchor_Number { get; set; }
    public int Identification_ID { get; set; }
    public int Sensor_ID { get; set; }

    public Vaartrack()
    {
        Blocked = 0;
        Length = 0;
        Length_Blocked = 0;
        Breadth = 0;
        Breadth_Blocked = 0;
        X_Position = 0;
        Y_Position = 0;
        Speed = 0;
        Speed_Blocked = 0;
        X_Speed = 0;
        X_Speed_Blocked = 0;
        Y_Speed = 0;
        Y_Speed_Blocked = 0;
        Orientation = 0;
        Orientation_Blocked = 0;
        Lat_Position_Track = "";
        Lon_Position_Track = "";
        LatPosition_Anchor = "";
        LonPosition_Anchor = "";
        UTC_Time = new DateTime();
        Anchor_Number = 0;
        Identification_ID = 0;
        Sensor_ID = 0;
    }

    public string toSQL()
    {
        return "Vaartrack VALUES('" +
               Blocked + "', '" +
               Length + "', '" +
               Length_Blocked + "', '" +
               Breadth + "', '" +
               Breadth_Blocked + "', '" +
               X_Position + "', '" +
               Y_Position + "', '" +
               Speed + "', '" +
               Speed_Blocked + "', '" +
               X_Speed + "', '" +
               X_Speed_Blocked + "', '" +
               Y_Speed + "', '" +
               Y_Speed_Blocked + "', '" +
               Orientation + "', '" +
               Orientation_Blocked + "', '" +
               Lat_Position_Track + "', '" +
               Lon_Position_Track + "', '" +
               LatPosition_Anchor + "', '" +
               LonPosition_Anchor + "', '" +
               UTC_Time + "', '" +
               Anchor_Number + "', '" +
               Identification_ID + "', '" +
               Sensor_ID + ")";
    }
}

